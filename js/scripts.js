var dinos = [];
var N = 5
var cont = 0;
var contframes = 0;
var delay = 3;

function setup (){
    createCanvas(400, 400);
    for (let i = 1; i <= N; i++){
        dinos.push(loadImage("imgs/Walk ("+i+").png"))
    }
}

function draw (){
    contframes++;
    if (!(contframes % delay)){
        background(16);
        let img = dinos [cont];
        image(img, -100+contframes, 10, img.width, img.height);
        cont++;
        if (cont >= N){
            cont = 0;
        }
        if (contframes >= 500){
            contframes = 0;
        }
    }
}